import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import BaseButton from '@/components/UI/BaseButton.vue'
import TodoForm from './components/TodoForm.vue'
import TodosCard from '@/components/UI/TodosCard.vue'


import './styles/app.css';

const app = createApp(App)

app.use(router)
app.component('base-button',BaseButton)
app.component('todo-form', TodoForm)
app.component('todos-card', TodosCard)


app.mount('#app')
