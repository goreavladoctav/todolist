
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyBTwBPReJXb4sB8njw3TJimcyobhNEXxVA",
  authDomain: "todolist-65228.firebaseapp.com",
  databaseURL: "https://todolist-65228-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "todolist-65228",
  storageBucket: "todolist-65228.appspot.com",
  messagingSenderId: "81791065053",
  appId: "1:81791065053:web:c163698944006ae32cf434"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app)

export {
  db
}