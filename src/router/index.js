import { createRouter, createWebHistory } from 'vue-router'
import ListTodos from '../components/ListTodos.vue'
import AddTodo from '../components/AddTodo.vue'

const routes = [
  {
    path: '/',
    name: 'todos',
    component: ListTodos
  },

  {
    path: '/add-todo',
    name: 'add-todo',
    component: AddTodo

  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
