/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'roboto': ['Roboto', 'sans-serif']
    },
    colors: {
      orange: '#F29422',
      white: '#F2F2F2',
      blue: '#298FA6',
      coral1: '#F28066',
      coral2: '#F27166',
      pinkGround: '#E3FFFB',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}